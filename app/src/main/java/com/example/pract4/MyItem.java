package com.example.pract4;

public class MyItem {
    private String text;
    private int imageId;

    public MyItem(String text, int imageId) {
        this.text = text;
        this.imageId = imageId;
    }

    public int getImageId() {
        return imageId;
    }

    public String getText() {
        return text;
    }
}
