package com.example.pract4;

import android.content.ClipData.Item;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.List;

public class MyListAdapter extends ArrayAdapter<MyItem> {

    private LayoutInflater inflater;
    private int layout;
    private List<MyItem> items;

    public MyListAdapter(@NonNull Context context, int resource, @NonNull List<MyItem> items) {
        super(context, resource, items);
        this.items = items;
        this.layout = resource;
        this.inflater = LayoutInflater.from(context);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view = inflater.inflate(this.layout, parent, false);
        TextView textView = view.findViewById(R.id.list_item_text_view);
        ImageView imageView = view.findViewById(R.id.list_item_image_view);
        MyItem item = items.get(position);
        textView.setText(item.getText());
        imageView.setImageResource(item.getImageId());
        return view;
    }
}
