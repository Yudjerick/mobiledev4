package com.example.pract4;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ItemsGenerator {
    public static List<MyItem> generate(int count){
        ArrayList<MyItem> list = new ArrayList<>(count);
        int[] imageIds = {R.drawable.fish1, R.drawable.spider3, R.drawable.snake2};

        Random random = new Random();
        for (int i = 0; i < count; i++) {
            MyItem item = new MyItem(String.valueOf(i), imageIds[random.nextInt(3)]);
            list.add(item);
        }

        return list;
    }
}
