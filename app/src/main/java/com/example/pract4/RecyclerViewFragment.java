package com.example.pract4;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RecyclerViewFragment extends Fragment {

    public RecyclerViewFragment() {
        // Required empty public constructor
    }

    public static RecyclerViewFragment newInstance(String param1, String param2) {
        RecyclerViewFragment fragment = new RecyclerViewFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_recycler_view, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        List<MyItem> items = ItemsGenerator.generate(200);
        MyItem[] itemsArr = {new MyItem("I", R.drawable.snake2),new MyItem("am", R.drawable.snake2),
                new MyItem("having", R.drawable.snake2), new MyItem("my", R.drawable.snake2),
                new MyItem("breakfast", R.drawable.snake2)};
        items = new ArrayList<>(Arrays.asList(itemsArr));

        RecyclerView itemsList = (RecyclerView) view.findViewById(R.id.recycler_view_list);
        MyRecyclerAdapter adapter = new MyRecyclerAdapter(getContext(), items);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        StaggeredGridLayoutManager layoutManager1 = new StaggeredGridLayoutManager(3,1);
        itemsList.setLayoutManager(layoutManager1);
        itemsList.setAdapter(adapter);

        Button go_back_btn = (Button) view.findViewById(R.id.go_back_btn2);
        go_back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager = getParentFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, MainFragment.class, null);
                fragmentTransaction.setReorderingAllowed(true);
                fragmentTransaction.commit();
            }
        });
    }
}