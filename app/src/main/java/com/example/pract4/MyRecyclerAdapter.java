package com.example.pract4;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.ViewHolder> {
    public static class ViewHolder extends RecyclerView.ViewHolder{
        public String TAG = "RecyclerAdapter";
        final TextView textView;
        final ImageView imageView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(imageView.getContext(), textView.getText(), Toast.LENGTH_SHORT).show();
                    Log.i(TAG, textView.getText().toString());
                }
            });
            this.textView = itemView.findViewById(R.id.list_item_text_view);
            this.imageView = itemView.findViewById(R.id.list_item_image_view);
        }
    }

    private final LayoutInflater inflater;
    private final List<MyItem> items;
    public MyRecyclerAdapter(Context context, List<MyItem> items) {
        this.inflater = LayoutInflater.from(context);
        this.items = items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.my_list_item, parent, false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        MyItem item = items.get(position);
        holder.textView.setText(item.getText());
        holder.imageView.setImageResource(item.getImageId());
    }
    @Override
    public int getItemCount() {
        return items.size();
    }
}
